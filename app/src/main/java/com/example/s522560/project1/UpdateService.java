package com.example.s522560.project1;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by S522560 on 10/29/2015.
 */
public class UpdateService extends Service {
    int mStartMode;       // indicates how to behave if the service is killed
    IBinder mBinder;      // interface for clients that bind
    boolean mAllowRebind; // indicates whether onRebind should be used

    //Date beforeTimeON;
    Calendar LogInTime , LogOutTime;
    MobileUsageDBHelper mydb;

    HashMap<Integer, String> rowName = new HashMap<>();

    @Override
    public void onCreate() {

        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        BroadcastReceiver mReceiver = new ScreenReceiver();
        registerReceiver(mReceiver, filter);
        LogInTime = Calendar.getInstance();
        rowName.put(0,"zero");
        rowName.put(1,"one");
        rowName.put(2,"two");
        rowName.put(3,"three");
        rowName.put(4,"four");
        rowName.put(5,"five");
        rowName.put(6,"six");
        rowName.put(7,"seven");
        rowName.put(8,"eight");
        rowName.put(9,"nine");
        rowName.put(10,"ten");
        rowName.put(11,"eleven");
        rowName.put(12,"twelve");
        rowName.put(13,"thirteen");
        rowName.put(14,"fourteen");
        rowName.put(15,"fifteen");
        rowName.put(16,"sixteen");
        rowName.put(17,"seventeen");
        rowName.put(18,"eighteen");
        rowName.put(19,"nineteen");
        rowName.put(20,"twenty");
        rowName.put(21,"twentyone");
        rowName.put(22,"twentytwo");
        rowName.put(23,"twentythree");




     }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        boolean screenOFF = intent.getBooleanExtra("screen_state", false);
        mydb = new MobileUsageDBHelper(this);
        if (screenOFF)
        {

            LogOutTime = Calendar.getInstance();

//            mydb.insertUsageData(LogInTime, LogOutTime);


            try {
                DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                Date today = new Date();
                Date todayWithZeroTime =formatter.parse(formatter.format(today));
                Log.d("ssss", String.valueOf(todayWithZeroTime));

                if(LogInTime.get(Calendar.HOUR_OF_DAY) == LogOutTime.get(Calendar.HOUR_OF_DAY))
                {
                    if((LogInTime.get(Calendar.YEAR) == LogOutTime.get(Calendar.YEAR)) &&
                            (LogInTime.get(Calendar.DAY_OF_YEAR) == LogOutTime.get(Calendar.DAY_OF_YEAR)) )
                    {
                        double val =   ((double) (LogOutTime.getTimeInMillis() - LogInTime.getTimeInMillis())) / (1000);
                        Log.d("ssss val1", String.valueOf(val));
                        mydb.insertUsageData(todayWithZeroTime, val, rowName.get(LogInTime.get(Calendar.HOUR_OF_DAY)));


                    }
                }
                else
                {
                    if((LogInTime.get(Calendar.YEAR) == LogOutTime.get(Calendar.YEAR)) &&
                            (LogInTime.get(Calendar.DAY_OF_YEAR) == LogOutTime.get(Calendar.DAY_OF_YEAR)) ) {

                        Calendar middleTime = LogOutTime;
                        middleTime.set(Calendar.MINUTE, 00);
                        double val = ((double) (middleTime.getTimeInMillis() - LogInTime.getTimeInMillis())) / (1000);
                        Log.d("ssss val2", String.valueOf(val));
                        val = ((double) (LogOutTime.getTimeInMillis() - middleTime.getTimeInMillis())) / (1000);
                        Log.d("ssss val3", String.valueOf(val));
                    }
                    else{

                    }

                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        else
        {
            LogInTime = Calendar.getInstance();

        }

       // The service is starting, due to a call to startService()
        return START_STICKY;
    }
    @Override
    public IBinder onBind(Intent intent) {
        // A client is binding to the service with bindService()
        return mBinder;
    }
    @Override
    public boolean onUnbind(Intent intent) {
        // All clients have unbound with unbindService()
        return mAllowRebind;
    }
    @Override
    public void onRebind(Intent intent) {
        // A client is binding to the service with bindService(),
        // after onUnbind() has already been called
    }
    @Override
    public void onDestroy() {
        // The service is no longer used and is being destroyed
    }



}
