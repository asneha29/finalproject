package com.example.s522560.project1.old;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

import com.example.s522560.project1.R;

public class TimeSetting extends AppCompatActivity {

    Button btn_start,btn_end;
    static final int Start_DIALOG_ID=0, End_DIALOG_ID=1;
    static int start_hour,start_minute, end_hour, end_minute ;
    SharedPreferences sharedPref;
    public static final String MyPREFERENCES = "MyPrefs" ;
    EditText statET;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        statET = (EditText) findViewById(R.id.startTimeHrET);
        btn_start = (Button) findViewById(R.id.SetStartBTN);
        btn_end = (Button) findViewById(R.id.SetEndBTN);
        sharedPref = getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);



        int savedH1 = sharedPref.getInt(getString(R.string.starttime_hour), 00);
        statET.setText(String.valueOf(savedH1));


        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(Start_DIALOG_ID);
            }
        });


        btn_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(End_DIALOG_ID);
            }
        });



    }

    @Override
    protected Dialog onCreateDialog(int id){
        if(id == Start_DIALOG_ID){
            return new TimePickerDialog(TimeSetting.this,StartTimePickerListener, start_hour, start_minute,false);
        }
        if(id == End_DIALOG_ID){
            return new TimePickerDialog(TimeSetting.this,EndTimePickerListener, start_hour, start_minute,false);
        }
        return null;
    }

    protected TimePickerDialog.OnTimeSetListener StartTimePickerListener = new TimePickerDialog.OnTimeSetListener(){

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            start_hour = hourOfDay;
            start_minute = minute;

            statET.setText(TimeSetting.start_hour + ":" + TimeSetting.start_minute);


            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt(getString(R.string.starttime_hour), TimeSetting.start_hour);
            editor.putInt(getString(R.string.starttime_minute), TimeSetting.start_minute);
            editor.commit();
        }
    };


    protected TimePickerDialog.OnTimeSetListener EndTimePickerListener = new TimePickerDialog.OnTimeSetListener(){

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            end_hour = hourOfDay;
            end_minute = minute;
            EditText statET = (EditText) findViewById(R.id.EndTimeET);
            statET.setText(TimeSetting.end_hour +":"+ TimeSetting.end_minute);
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sub_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }


}