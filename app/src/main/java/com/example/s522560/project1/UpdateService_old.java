package com.example.s522560.project1;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by S522560 on 10/29/2015.
 */
public class UpdateService_old extends Service {
    int mStartMode;       // indicates how to behave if the service is killed
    IBinder mBinder;      // interface for clients that bind
    boolean mAllowRebind; // indicates whether onRebind should be used

    //Date beforeTimeON;
    Long LogInTime , LogOutTime;
    MobileUsageDBHelper mydb;


    @Override
    public void onCreate() {

        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        BroadcastReceiver mReceiver = new ScreenReceiver();
        registerReceiver(mReceiver, filter);
        LogInTime = Calendar.getInstance().getTimeInMillis();
     }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        boolean screenOFF = intent.getBooleanExtra("screen_state", false);
        mydb = new MobileUsageDBHelper(this);
        if (screenOFF)
        {

            LogOutTime = Calendar.getInstance().getTimeInMillis();
           // mydb.insertUsageData(LogInTime, LogOutTime);
            Date mydate = new Date(LogInTime);
            Log.d("ssss", String.valueOf(mydate));
        }
        else
        {
            LogInTime = Calendar.getInstance().getTimeInMillis();

        }

       // The service is starting, due to a call to startService()
        return START_STICKY;
    }
    @Override
    public IBinder onBind(Intent intent) {
        // A client is binding to the service with bindService()
        return mBinder;
    }
    @Override
    public boolean onUnbind(Intent intent) {
        // All clients have unbound with unbindService()
        return mAllowRebind;
    }
    @Override
    public void onRebind(Intent intent) {
        // A client is binding to the service with bindService(),
        // after onUnbind() has already been called
    }
    @Override
    public void onDestroy() {
        // The service is no longer used and is being destroyed
    }

}
