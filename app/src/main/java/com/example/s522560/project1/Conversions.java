package com.example.s522560.project1;

/**
 * Created by S522560 on 12/1/2015.
 */
public class Conversions {

    public static String convertSecToHMS(double totalSeconds){

        final int MINUTES_IN_AN_HOUR = 60;
        final int SECONDS_IN_A_MINUTE = 60;

        int seconds = (int) totalSeconds % SECONDS_IN_A_MINUTE;
        int totalMinutes = (int) totalSeconds / SECONDS_IN_A_MINUTE;
        int minutes = totalMinutes % MINUTES_IN_AN_HOUR;
        int hours = totalMinutes / MINUTES_IN_AN_HOUR;

        String s = "";
        if(hours<10){
            s += "0"+hours+":";
        }
        else{
            s+=hours+":";
        }

        if(minutes<10){
            s += "0"+minutes+":";
        }
        else{
            s+=minutes+":";
        }


        if(seconds<10){
            s += "0"+seconds;
        }
        else{
            s+=seconds;
        }
        return s;
    }


    public static String convertSecToHrs(double totalSeconds){

        final int MINUTES_IN_AN_HOUR = 60;
        final int SECONDS_IN_A_MINUTE = 60;

        int seconds = (int) totalSeconds % SECONDS_IN_A_MINUTE;
        int totalMinutes = (int) totalSeconds / SECONDS_IN_A_MINUTE;
        int minutes = totalMinutes % MINUTES_IN_AN_HOUR;
        int hours = totalMinutes / MINUTES_IN_AN_HOUR;


        return hours+" Hrs "+minutes+" Mins "+seconds + " Secs ";
    }
}
