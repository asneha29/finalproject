package com.example.s522560.project1;

/**
 * Created by S522560 on 10/30/2015.
 */
public class MobileUsage {
    private String date;
    private double timeInSec;

    public MobileUsage(String date, double timeInSec) {
        this.date = date;
        this.timeInSec = timeInSec;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getTimeInSec() {
        return timeInSec;
    }

    public void setTimeInSec(double timeInSec) {
        this.timeInSec = timeInSec;
    }

    public String toString(){
       return String.valueOf(date) + " " + String.valueOf(timeInSec);
    }
}
