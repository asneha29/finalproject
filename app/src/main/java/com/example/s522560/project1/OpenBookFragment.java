package com.example.s522560.project1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class OpenBookFragment extends Fragment implements View.OnClickListener {

    private String url;

    public static OpenBookFragment newInstance(String url) {
        OpenBookFragment fragment=new OpenBookFragment();
        fragment.url=url;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_open_book, container, false);

        Button backBTN=(Button)v.findViewById(R.id.goBackButton);
        backBTN.setOnClickListener(this);

        WebView bookWV=(WebView)v.findViewById(R.id.bookWV);
        bookWV.getSettings().setJavaScriptEnabled(true);



        bookWV.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);
                return true;
            }
        });

        bookWV.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + url);
        return v;
    }

    @Override
    public void onClick(View v) {
        BookFragment bFrgament=new BookFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.container, bFrgament)
                .commit();
    }
}
