package com.example.s522560.project1;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

public class AppSetting extends BaseActivity {


    LinearLayout btn_start,btn_end;
    static final int Start_DIALOG_ID=0, End_DIALOG_ID=1;
    static int start_hour,start_minute, end_hour, end_minute ;
    String s1, s2;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    TextView startHrET ,endHrET,endMinET, startMinET;
    TextView startTimeText, endTimeText;
    EditText name;

    private static String TimePREFERENCES="MyTimePref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        sharedPref = getSharedPreferences(TimePREFERENCES, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        timeSetting();
        nameSetting();
    }

    public void nameSetting(){
        name = (EditText) findViewById(R.id.userNameET);
        String n = sharedPref.getString("userName","");

        name.setText(n);

        name.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                System.out.println("userName: " + String.valueOf(s));
                String s1 = String.valueOf(s);
                editor.putString("userName", s1);
                editor.commit();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {

                }
            }
        });

    }

    public void timeSetting(){

        startHrET = (TextView) findViewById(R.id.startTimeHrET);
        endHrET = (TextView) findViewById(R.id.endTimeHrET);
        startMinET = (TextView) findViewById(R.id.startTimeMinET);
        endMinET = (TextView) findViewById(R.id.endTimeMinET);
        startTimeText = (TextView) findViewById(R.id.startTimeText);
        endTimeText = (TextView) findViewById(R.id.endTimeText);


        btn_start = (LinearLayout) findViewById(R.id.SetStartBTN);
        btn_end = (LinearLayout) findViewById(R.id.SetEndBTN);


//
        start_hour = sharedPref.getInt(getString(R.string.starttime_hour), 00);
        start_minute = sharedPref.getInt(getString(R.string.starttime_minute), 00);
        end_hour = sharedPref.getInt(getString(R.string.endtime_hour), 00);
        end_minute = sharedPref.getInt(getString(R.string.endtime_minute), 00);
        s1 = sharedPref.getString("StartAM-PM", " ");
        s2 = sharedPref.getString("EndAM-PM", " ");

        startHrET.setText(getTimeFormat(get12HrTimeFormat(start_hour)));
        endHrET.setText(getTimeFormat(get12HrTimeFormat(end_hour)));
        startMinET.setText(getTimeFormat(start_minute));
        endMinET.setText(getTimeFormat(end_minute));
        startTimeText.setText(s1);
        endTimeText.setText(s2);


        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showDialog(Start_DIALOG_ID);
                new TimePickerDialog(AppSetting.this, StartTimePickerListener, start_hour, start_minute, false).show();
            }
        });


        btn_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(AppSetting.this, EndTimePickerListener, start_hour, start_minute, false).show();
            }
        });


    }

    protected TimePickerDialog.OnTimeSetListener StartTimePickerListener = new TimePickerDialog.OnTimeSetListener(){

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            start_hour = hourOfDay;
            start_minute = minute;

            editor.putInt(getString(R.string.starttime_hour), AppSetting.start_hour);
            editor.putInt(getString(R.string.starttime_minute), AppSetting.start_minute);
            Log.d("sss", String.valueOf(start_hour));
            Log.d("sss", String.valueOf(start_minute));
            String state="AM";
            if(start_hour > 12){
                state = "PM";
            }
            startHrET.setText(getTimeFormat(get12HrTimeFormat(AppSetting.start_hour)));
            startMinET.setText(getTimeFormat(AppSetting.start_minute));
            startTimeText.setText(state);
            editor.putString("StartAM-PM", state);
            editor.commit();
        }
    };


    protected TimePickerDialog.OnTimeSetListener EndTimePickerListener = new TimePickerDialog.OnTimeSetListener(){

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            end_hour = hourOfDay;
            end_minute = minute;

            editor.putInt(getString(R.string.endtime_hour), AppSetting.end_hour);
            editor.putInt(getString(R.string.endtime_minute), AppSetting.end_minute);

            String state ="AM";
            if(end_hour > 12){
                state = "PM";
            }
            endHrET.setText(getTimeFormat(get12HrTimeFormat(AppSetting.end_hour)));
            endMinET.setText(getTimeFormat(AppSetting.end_minute));
            endTimeText.setText(state);

            editor.putString("EndAM-PM", state);
            editor.commit();
        }
    };

    public static String getTimeFormat(int value){

        if(value > 9)
        {
            return String.valueOf(value);
        }
        else{
            String s = "0"+String.valueOf(value);
            return s;
        }
    }
    public  static int get12HrTimeFormat(int value){

        if(value > 12)
        {
            return value-12;
        }
        else{

            return value;
        }
    }


}
