package com.example.s522560.project1;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.Path;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SheepJump.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SheepJump#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SheepJump extends Fragment {


    private OnFragmentInteractionListener mListener;
    AnimationDrawable rocketAnimation;
    ObjectAnimator objectAnimator;
    ImageView floatingImage;
    TextView  sheepCNT;
    int sheep_counter =0;
    int first =0;

    View v;


    // TODO: Rename and change types and number of parameters
    public static SheepJump newInstance(String param1, String param2) {
        SheepJump fragment = new SheepJump();
        return fragment;
    }

    public SheepJump() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         v = inflater.inflate(R.layout.fragment_sheep_jump, container, false);
        floatingImage = (ImageView) v.findViewById(R.id.sheep);


        final AnimatorSet animatorSet = new AnimatorSet();

        float x = -170;
        float y = 960;

        Path path = new Path();
        System.out.println(x);
        path.moveTo(x, y);
        path.lineTo(x + 200, y - 150);
        path.lineTo(x + 350, y - 180);
        path.lineTo(x + 470, y - 400);
        path.lineTo(x + 550, y - 390);
        path.lineTo(x + 600, y - 380);
        path.lineTo(x + 1000, y - 180);


        objectAnimator =  ObjectAnimator.ofFloat(floatingImage, View.X, View.Y, path);
        objectAnimator.setDuration(6000);
        objectAnimator.setRepeatCount(Animation.INFINITE);


        Button btn = (Button) v.findViewById(R.id.StartBTN);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(first ==0 ){
                    floatingImage.setImageResource(R.drawable.final_sheep);
                    objectAnimator.start();
                    first = 1;
                }
                else{
                    objectAnimator.resume();
                }

            }
        });

        Button btn1 = (Button) v.findViewById(R.id.StopBTN);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectAnimator.pause();
            }
        });

        Animator.AnimatorListener animatorListener
                = new Animator.AnimatorListener() {

            public void onAnimationStart(Animator animation) {
//                setStatusTexts();
//                messageText.setText("started");
            }

            public void onAnimationRepeat(Animator animation) {
//                setStatusTexts();
//                messageText.setText("repeating");
                sheepCNT = (TextView) v.findViewById(R.id.sheepcount);
                sheep_counter++;
                sheepCNT.setText(String.valueOf(sheep_counter));
            }

            public void onAnimationEnd(Animator animation) {
//                setStatusTexts();
//                messageText.setText(messageText.getText()+" -- ended");
            }
            public void onAnimationCancel(Animator animation) {
//                setStatusTexts();
//                messageText.setText("cancelled");
            }
        };

        objectAnimator.addListener(animatorListener);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
