package com.example.s522560.project1;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.achartengine.ChartFactory;
import org.achartengine.chart.BarChart;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


public class GraphFragment extends Fragment {

    SharedPreferences sharedPref;
    private static String TimePREFERENCES="MyTimePref";
    private WebView webview ,webview1;
    TextView timeDiff, actualTimediff;



    public GraphFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
     //    Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_graph, container, false);

        ArrayList<String>  titles = new ArrayList<>();

        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

        Calendar today = Calendar.getInstance();

        for(int i=1 ; i<= 5 ; i++) {
            today.add(Calendar.DATE, -1);
            String currentDate_parse = formatter.format(today.getTime());

            titles.add(currentDate_parse);
        }


        MobileUsageDBHelper db = new MobileUsageDBHelper(getContext());
        List<double[]> values = new ArrayList<>();
        try {
            values = db.getPast5DaysData();
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        values.add(new double[] { 0.5, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1, 0.5, 0.4});
//        values.add(new double[] { 0.3, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1, 0.5, 0.4});
//        values.add(new double[] { 0.2, 0.5, 0.5, 0.4, 0.5, 0.2, 0.5, 0.4, 0.5, 0.3, 0.5, 0.4, 0.5, 0.4, 0.5, 0.4, 0.5, 0.1, 0.5, 0.4, 0.5, 0, 0.5, 0.4});
//        values.add(new double[] { 0.9, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1, 0.5, 0.4});
//        values.add(new double[]{  1,   1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1,   0.5, 0.4, 0.5, 1, 0.5, 0.4});


        int[] colors = new int[] { Color.parseColor("#ffcc5c"), Color.parseColor("#35a79c"), Color.parseColor("#ee4035"),Color.parseColor("#7bc043"), Color.parseColor("#0392cf") };

        XYMultipleSeriesRenderer renderer = buildBarRenderer(colors);
        setChartSettings(renderer, "", "Hours", "Spent Minutes", 0, 25, 0, 5, Color.WHITE, Color.LTGRAY);

        //renderer.setDisplayChartValues(true);
        timeDiff = (TextView) v.findViewById(R.id.TimediffET);
        actualTimediff = (TextView) v.findViewById(R.id.actualTimediffET);;
        double targetvalue = getTimediff();
        double actualvalue=0.0;
        try {
             actualvalue =ActualSleepTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //System.out.println("newValue: " + Conversions.convertSecToHrs(x * 60));

        timeDiff.setText(Conversions.convertSecToHrs(targetvalue));
        actualvalue = targetvalue - actualvalue;
        actualTimediff.setText(Conversions.convertSecToHrs(actualvalue));


        renderer.setXLabels(0);
        renderer.setYLabels(15);
        renderer.setPanEnabled(false, false);
        renderer.setPanEnabled(true, true);
        renderer.setZoomEnabled(true, true);
        renderer.setXLabelsAlign(Paint.Align.LEFT);
        renderer.setYLabelsAlign(Paint.Align.RIGHT);
        renderer.setMargins(new int[]{80, 30, 70, 30});

        renderer.setBarSpacing(0.5f);
        renderer.setXLabelsAngle(40);


        renderer.addXTextLabel(1, "00-01");
        renderer.addXTextLabel(2, "01-02");
        renderer.addXTextLabel(3, "02-03");
        renderer.addXTextLabel(4, "03-04");
        renderer.addXTextLabel(5, "04-05");
        renderer.addXTextLabel(6, "05-06");
        renderer.addXTextLabel(7, "06-07");
        renderer.addXTextLabel(8, "07-08");
        renderer.addXTextLabel(9, "08-09");
        renderer.addXTextLabel(10, "09-10");
        renderer.addXTextLabel(11, "10-11");
        renderer.addXTextLabel(12, "11-12");
        renderer.addXTextLabel(13, "12-13");
        renderer.addXTextLabel(14, "13-14");
        renderer.addXTextLabel(15, "14-15");
        renderer.addXTextLabel(16, "15-16");
        renderer.addXTextLabel(17, "16-17");
        renderer.addXTextLabel(18, "17-18");
        renderer.addXTextLabel(19, "18-19");
        renderer.addXTextLabel(20, "19-20");
        renderer.addXTextLabel(21, "20-21");
        renderer.addXTextLabel(22, "21-22");
        renderer.addXTextLabel(23, "22-23");
        renderer.addXTextLabel(24, "23-24");
        renderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00));
        XYMultipleSeriesDataset dataset = buildBarDataset(titles, values);

        View view = ChartFactory.getBarChartView(getActivity(), dataset, renderer, BarChart.Type.HEAPED); //Type.STACKED
        //view.setBackgroundColor(Color.parseColor("#3d3d3d"));
        LinearLayout myLayout = (LinearLayout) v.findViewById(R.id.chart);
        myLayout.addView(view);
        return v;

    }

    protected XYMultipleSeriesDataset buildBarDataset(ArrayList<String> titles, List<double[]> values)
    {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        int length = titles.size();
        for (int i = 0; i < length; i++)
        {
            CategorySeries series = new CategorySeries(titles.get(i));
            double[] v = values.get(i);
            int seriesLength = v.length;
            for (int k = 0; k < seriesLength; k++)
            {
                BigDecimal rval = new BigDecimal(v[k]).setScale(2,BigDecimal.ROUND_HALF_UP);
                series.add(rval.doubleValue());
            }
            dataset.addSeries(series.toXYSeries());
        }
        for (int i = 0; i < 24; i++)
        {
            XYSeries XYseries = dataset.getSeriesAt(0);
            double val = 0.0;
            for (int k = 0; k < 5; k++)
            {
                double[] v = values.get(k);
                val += v[i];
            }
            System.out.println("myval: " + val);
            BigDecimal tval = new BigDecimal(val).setScale(2,BigDecimal.ROUND_HALF_UP);
            XYseries.addAnnotation(String.valueOf(tval.doubleValue()), i+1,tval.doubleValue()+0.02);
        }
        return dataset;
    }

    protected XYMultipleSeriesRenderer buildBarRenderer(int[] colors)
    {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
        renderer.setAxisTitleTextSize(12);
        renderer.setChartTitleTextSize(20);
        renderer.setLabelsTextSize(15);
        renderer.setLegendTextSize(15);
        int length = colors.length;
        for (int i = 0; i < length; i++) {
            SimpleSeriesRenderer r = new XYSeriesRenderer();
            r.setColor(colors[i]);
            renderer.addSeriesRenderer(r);
        }
        return renderer;
    }

    protected void setChartSettings(XYMultipleSeriesRenderer renderer, String title, String xTitle, String yTitle, double xMin, double xMax, double yMin, double yMax, int axesColor, int labelsColor)
    {
        renderer.setChartTitle(title);
        renderer.setXTitle("\n\n\n\n" +xTitle);
        renderer.setYTitle(yTitle);
        renderer.setXAxisMin(xMin);
        renderer.setXAxisMax(xMax);
        renderer.setYAxisMin(yMin);
        renderer.setYAxisMax(yMax);
        renderer.setAxesColor(axesColor);
        renderer.setLabelsColor(labelsColor);
        renderer.setDisplayValues(true);
    }

    public double ActualSleepTime() throws ParseException {

        HashMap<Integer, String> rowName = new HashMap<>();
        rowName.put(0,"zero");
        rowName.put(1,"one");
        rowName.put(2,"two");
        rowName.put(3,"three");
        rowName.put(4,"four");
        rowName.put(5,"five");
        rowName.put(6,"six");
        rowName.put(7,"seven");
        rowName.put(8,"eight");
        rowName.put(9,"nine");
        rowName.put(10,"ten");
        rowName.put(11,"eleven");
        rowName.put(12,"twelve");
        rowName.put(13,"thirteen");
        rowName.put(14,"fourteen");
        rowName.put(15,"fifteen");
        rowName.put(16,"sixteen");
        rowName.put(17,"seventeen");
        rowName.put(18,"eighteen");
        rowName.put(19,"nineteen");
        rowName.put(20,"twenty");
        rowName.put(21,"twentyone");
        rowName.put(22,"twentytwo");
        rowName.put(23,"twentythree");
        SharedPreferences sharedPref = getActivity().getSharedPreferences("MyTimePref", Context.MODE_PRIVATE);

        int start_hour = sharedPref.getInt(getString(R.string.starttime_hour), 00);
        int end_hour = sharedPref.getInt(getString(R.string.endtime_hour), 00);
        ArrayList<String> rows = new ArrayList<>();


        if(start_hour <= end_hour){
            for(int i=start_hour; i<end_hour; i++){
                rows.add(rowName.get(i));
            }
        }
        else{
            for(int i=start_hour; i<24; i++){
                rows.add(rowName.get(i));
            }
            for(int i=0; i<end_hour; i++){
                rows.add(rowName.get(i));
            }

        }
        MobileUsageDBHelper db = new MobileUsageDBHelper(getContext());
        double result = db.getActualSleepTime(rows);
        System.out.println("NewValue: "+ result);
        return (result);

    }


    public double getTimediff(){

        sharedPref = getActivity().getSharedPreferences(TimePREFERENCES, Context.MODE_PRIVATE);

        int start_hour = sharedPref.getInt(getString(R.string.starttime_hour), 00);
        int start_minute = sharedPref.getInt(getString(R.string.starttime_minute), 00);
        int end_hour = sharedPref.getInt(getString(R.string.endtime_hour), 00);
        int end_minute = sharedPref.getInt(getString(R.string.endtime_minute), 00);

        int diffMin;
        if (end_minute < start_minute) {
            diffMin = 60 + end_minute - start_minute;
            if (end_hour == 0) {
                end_hour = 23;
            } else {
                end_hour--;
            }
        } else {
            diffMin = end_minute - start_minute;
        }

        int diffHour;
        if (end_hour < start_hour) {
            diffHour = end_hour + 24 - start_hour;
        } else {
            diffHour = end_hour - start_hour;
        }

        int v = diffHour *60 ;
        int x = v+diffMin;
        x = x*5;

        return (x*60);
    }




}
