package com.example.s522560.project1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by S522560 on 10/30/2015.
 */
public class MobileUsageDBHelper_old extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "InsomniacApp.db";
    public static final String TABLE_NAME= "IN_MobileUsage_full";
    static final String DATABASE_CREATE =
            "create table "+TABLE_NAME +" (LogIn long not null,LogOut long not null, TimeSpent double not null);";

    public MobileUsageDBHelper_old(Context context)
    {

        super(context, DATABASE_NAME , null, 1);
    }

//
//    public MobileUsageDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
//        super(context, name, factory, version);
//    }
//
//    public MobileUsageDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
//        super(context, name, factory, version, errorHandler);
//    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE); // creates tables & initializes them

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("TAG", "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(db);
    }

    public boolean insertUsageData  (long logIn, long logOut)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("LogIn", logIn);
        contentValues.put("LogOut", logOut);
        double theCalSeconds = ((double) (logOut - logIn)) / (1000);
        contentValues.put("TimeSpent", theCalSeconds);
        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }



    public ArrayList<MobileUsage> getPast5DaysData(){
        ArrayList<MobileUsage> array_list = new ArrayList<MobileUsage>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar t1 = Calendar.getInstance();

        String time1 = dateFormat.format(t1.getTime());
        Log.d("time1", time1);

        t1.add(Calendar.DATE, -6);

        String time2 = dateFormat.format(t1.getTime());
        Log.d("time2", time2);

        String selectQuery = "Select date(LogIn/1000,'unixepoch','localtime') as date , sum(TimeSpent) as timespent from "+TABLE_NAME+" where date(LogIn/1000,'unixepoch','localtime') between '"+time2+"' and '"+time1+"' group by date(LogIn / 1000, 'unixepoch', 'localtime'); ";
        Cursor res =  db.rawQuery( selectQuery, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(new MobileUsage(res.getString(res.getColumnIndex("date")),res.getDouble(res.getColumnIndex("timespent"))));
            res.moveToNext();
        }
        return array_list;
    }

    public double getTodayData(){
        ArrayList<MobileUsage> array_list = new ArrayList<MobileUsage>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar t1 = Calendar.getInstance();

        String time1 = dateFormat.format(t1.getTime());
        Log.d("time1", time1);

        String selectQuery = "Select date(LogIn/1000,'unixepoch','localtime') as date , sum(TimeSpent) as timespent from "+TABLE_NAME+" where date(LogIn/1000,'unixepoch','localtime') ='"+time1+"' group by date(LogIn / 1000, 'unixepoch', 'localtime'); ";
        Cursor res =  db.rawQuery( selectQuery, null );
        res.moveToFirst();
        double x =0;
        while(res.isAfterLast() == false){
            x = res.getDouble(res.getColumnIndex("timespent"));
            res.moveToNext();
        }
        return x;
    }




}
