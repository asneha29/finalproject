package com.example.s522560.project1;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MusicFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MusicFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MusicFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    private Map<String,Integer> songsList=new TreeMap<>();
    private ListView songsView;
    private MediaPlayer mp;
    public TextView duration;
    private double timeElapsed = 0, finalTime = 0;
    private Handler durationHandler = new Handler();
    private SeekBar seekbar;
    int i=0;
    ImageButton play;
    TextView titleTextView;

    public static MusicFragment newInstance(String param1, String param2) {
        MusicFragment fragment = new MusicFragment();

        return fragment;
    }

    public MusicFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v=  inflater.inflate(R.layout.fragment_music, container, false);

        songsView=(ListView) v.findViewById(R.id.songsList);
        titleTextView = (TextView) v.findViewById(R.id.SongTitle);

        addSongs();
        mp = new MediaPlayer();

        final String[] songTitles=songsList.keySet().toArray(new String[songsList.size()]);
        mp = MediaPlayer.create(getActivity(),songsList.get(songTitles[0]));

        ArrayAdapter<String> songAdt = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,songTitles);
        songsView.setAdapter(songAdt);

        seekbar = (SeekBar) v.findViewById(R.id.seekBar);
        songsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                i = position;
                String title = parent.getAdapter().getItem(position).toString();
                skipToNext(title);
                titleTextView.setText(title);

                finalTime = mp.getDuration();
                duration = (TextView) v.findViewById(R.id.songDuration);

                seekbar.setMax((int) finalTime);
                seekbar.setClickable(true);
                timeElapsed = mp.getCurrentPosition();
                seekbar.setProgress((int) timeElapsed);
                durationHandler.postDelayed(updateSeekBarTime, 100);
                play.setImageResource(R.drawable.ic_action_pause);
            }
        });

        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                ++i;
                songsView.performItemClick(songsView.getAdapter().getView(i, null, null), i, i);
            }
        });

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                                               @Override
                                               public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                                   if (fromUser) {
                                                       mp.seekTo(progress);
                                                   }
                                               }

                                               @Override
                                               public void onStartTrackingTouch(SeekBar seekBar) {}

                                               @Override
                                               public void onStopTrackingTouch(SeekBar seekBar) {}
                                           }
        );


        play = (ImageButton) v.findViewById(R.id.playBTN);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mp.isPlaying()){
                    mp.pause();
                    play.setImageResource(R.drawable.ic_action_play);
                }else{
                    mp.start();
                    play.setImageResource(R.drawable.ic_action_pause);
                }
            }
        });

        ImageButton nextSong = (ImageButton) v.findViewById(R.id.fwdBTN);
        nextSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i++;
                if (i >songsList.size()-1) {
                    i=0;
                }
                songsView.performItemClick(songsView.getAdapter().getView(i, null, null), i, i);
            }
        });

        ImageButton prevsong = (ImageButton) v.findViewById(R.id.backBTN);
        prevsong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i--;
                if(i<0) {
                    i=songsList.size()-1;
                }
                songsView.performItemClick(
                        songsView.getAdapter().getView(i, null, null), i, i);
            }
        });

        return v;
    }

    //handler to change seekBarTime
    private Runnable updateSeekBarTime = new Runnable() {
        public void run() {
            //get current position
            timeElapsed = mp.getCurrentPosition();
            //set seekbar progress
            seekbar.setProgress((int) timeElapsed);
            //set time remaing
            double timeRemaining = finalTime - timeElapsed;
            duration.setText(String.format("%d mins, %d secs ", TimeUnit.MILLISECONDS.toMinutes((long) timeRemaining), TimeUnit.MILLISECONDS.toSeconds((long) timeRemaining) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) timeRemaining))));

            //repeat yourself that again in 100 miliseconds
            durationHandler.postDelayed(this, 100);
        }
    };


    public void skipToNext(String songTitle){
        if (mp.isPlaying()){
            mp.stop();
            mp.seekTo(0);
        }
        mp=MediaPlayer.create(getActivity(),songsList.get(songTitle));
        mp.start();
    }



    public void addSongs(){
        songsList.put("Clean Relaxing", R.raw.clean_relaxing_music);
        songsList.put("A Minor Thing", R.raw.a_minor_thing);
        songsList.put("Fall Soft",R.raw.fall_soft_music);
        songsList.put("Angel voice",R.raw.angel_voice);

//        songsList.put("Water Ambient",R.raw.water_ambient);
//        songsList.put("Moon and littlestars", R.raw.moon_and_little_stars);
//        songsList.put("Moonstone ambient easy", R.raw.moonstone_ambient_easy_listening_music);
//        songsList.put("Nirvana relaxing piano music", R.raw.nirvana_relaxing_piano_music);
//        songsList.put("Relax song", R.raw.relax_song);
//        songsList.put("To Dreams Unsettled", R.raw.to_dreams_unsettled);
//        songsList.put("Pentatonic waves", R.raw.pentatonic_waves);
    }

    @Override
    public void onDestroy() {
        if (mp.isPlaying())
            mp.stop();
        super.onDestroy();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
