package com.example.s522560.project1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by S522560 on 10/30/2015.
 */
public class MobileUsageDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "InsomniacApp.db";
    public static final String TABLE_NAME= "IN_MobileUsage_full";
    static final String DATABASE_CREATE =
            "create table "+TABLE_NAME +" (Date date not null,one double DEFAULT 0,zero double DEFAULT 0, two double DEFAULT 0,three double DEFAULT 0, four double DEFAULT 0, five double DEFAULT 0, six double DEFAULT 0, seven double DEFAULT 0, eight double DEFAULT 0, nine double DEFAULT 0, ten double DEFAULT 0, eleven double DEFAULT 0 , twelve double DEFAULT 0, thirteen double DEFAULT 0, fourteen double DEFAULT 0, fifteen double DEFAULT 0, sixteen double DEFAULT 0, seventeen double DEFAULT 0, eighteen double DEFAULT 0, nineteen double DEFAULT 0, twenty double DEFAULT 0, twentyone double DEFAULT 0, twentytwo double DEFAULT 0, twentythree double DEFAULT 0  );";

    public MobileUsageDBHelper(Context context)
    {
        super(context, DATABASE_NAME , null, 1);
    }

//
//    public MobileUsageDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
//        super(context, name, factory, version);
//    }
//
//    public MobileUsageDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
//        super(context, name, factory, version, errorHandler);
//    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE); // creates tables & initializes them

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("TAG", "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(db);
    }

    public boolean insertUsageData  (Date date,double val, String rowname)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        String sql ="SELECT zero FROM "+TABLE_NAME+" WHERE date='"+date+"'";
        Cursor cursor= db.rawQuery(sql,null);

        if(cursor.getCount()>0){
           // contentValues.put("", String.valueOf(date));
            db.execSQL("UPDATE " + TABLE_NAME + " SET "
                    + rowname + " =" + rowname + "+"+val+" WHERE date"
                    + "='" +date+"'");
        }else{
            contentValues.put("date", String.valueOf(date));
            db.insert(TABLE_NAME, null, contentValues);

            contentValues = new ContentValues();
            contentValues.put(rowname, val);

            //db.update(TABLE_NAME, contentValues, date + " = '" + date +"'",null);
            db.execSQL("UPDATE " + TABLE_NAME + " SET "
                    + rowname + " = " + rowname + "+"+val+" WHERE date"
                    + "='" +date +"'");

        }
//        contentValues.put("LogIn", logIn);
//        contentValues.put("LogOut", logOut);
//        double theCalSeconds = ((double) (logOut - logIn)) / (1000);
//        contentValues.put("TimeSpent", theCalSeconds);
//        db.insert(TABLE_NAME, null, contentValues);
        db.close();
        return true;
    }



    public List<double[]> getPast5DaysData() throws ParseException
    {
        ArrayList<MobileUsage> array_list = new ArrayList<MobileUsage>();
        List<double[]> values = new ArrayList<double[]>();
        //hp = new HashMap();

        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Calendar today = Calendar.getInstance();
        Date todayWithZeroTime =formatter.parse(formatter.format(today.getTime()));

        for(int i=1 ; i<= 5 ; i++)
        {
            today.add(Calendar.DATE, -1);
            Date currentDate_parse =formatter.parse(formatter.format(today.getTime()));

            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "Select * from "+TABLE_NAME+" where date='" + currentDate_parse+"'";
            Cursor res =  db.rawQuery( selectQuery, null );
            res.moveToFirst();
           int flag =0;
            while(res.isAfterLast() == false)
            {
                flag=1;
                BigDecimal zero = new BigDecimal(((res.getDouble(res.getColumnIndex("zero")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal one = new BigDecimal(((res.getDouble(res.getColumnIndex("one")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal two = new BigDecimal(((res.getDouble(res.getColumnIndex("two")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal three = new BigDecimal(((res.getDouble(res.getColumnIndex("three")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal four = new BigDecimal(((res.getDouble(res.getColumnIndex("four")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal five = new BigDecimal(((res.getDouble(res.getColumnIndex("five")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal six = new BigDecimal(((res.getDouble(res.getColumnIndex("six")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal seven = new BigDecimal(((res.getDouble(res.getColumnIndex("seven")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal eight = new BigDecimal(((res.getDouble(res.getColumnIndex("eight")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal nine = new BigDecimal(((res.getDouble(res.getColumnIndex("nine")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal ten = new BigDecimal(((res.getDouble(res.getColumnIndex("ten")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal eleven = new BigDecimal(((res.getDouble(res.getColumnIndex("eleven")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal twelve = new BigDecimal(((res.getDouble(res.getColumnIndex("twelve")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal thirteen = new BigDecimal(((res.getDouble(res.getColumnIndex("thirteen")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal fourteen = new BigDecimal(((res.getDouble(res.getColumnIndex("fourteen")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal fifteen = new BigDecimal(((res.getDouble(res.getColumnIndex("fifteen")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal sixteen = new BigDecimal(((res.getDouble(res.getColumnIndex("sixteen")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal seventeen = new BigDecimal(((res.getDouble(res.getColumnIndex("seventeen")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal eighteen = new BigDecimal(((res.getDouble(res.getColumnIndex("eighteen")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal nineteen = new BigDecimal(((res.getDouble(res.getColumnIndex("nineteen")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal twenty = new BigDecimal(((res.getDouble(res.getColumnIndex("twenty")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal twentyone = new BigDecimal(((res.getDouble(res.getColumnIndex("twentyone")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal twentytwo = new BigDecimal(((res.getDouble(res.getColumnIndex("twentytwo")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
                BigDecimal twentythree = new BigDecimal(((res.getDouble(res.getColumnIndex("twentythree")))/(60*60))).setScale(2,BigDecimal.ROUND_HALF_UP);
             System.out.println ("zero " + zero.doubleValue()+ " one " + one.doubleValue() + " two " +
                     two.doubleValue() + " three " +
                     three.doubleValue() + " four " +
                     four.doubleValue() + "five " +
                     five.doubleValue() + " six " +
                     six.doubleValue() + " seven " +
                     seven.doubleValue() + " eight " +
                     eight.doubleValue() + " nine " +
                     nine.doubleValue() + " ten " +
                     ten.doubleValue() + " eleven " +
                     eleven.doubleValue() + " twelve " +
                     twelve.doubleValue() + " t13 " +
                     thirteen.doubleValue() + " t14 " +
                     fourteen.doubleValue() + " t14 " +
                     fifteen.doubleValue() + " t15 " +
                     sixteen.doubleValue() + " t16 " +
                     seventeen.doubleValue() + " t17 " +
                     eighteen.doubleValue() + " t18 " +
                     nineteen.doubleValue() + " t19 " +
                     twenty.doubleValue() + " t20 " +
                     twentyone.doubleValue() + " t21 " +
                     twentytwo.doubleValue() + " t22 " +
                     twentythree.doubleValue() + " " );
               values.add(new double[] {
                      zero.doubleValue(),
                      one.doubleValue(),
                       two.doubleValue(),
                       three.doubleValue(),
                       four.doubleValue(),
                       five.doubleValue(),
                       six.doubleValue(),
                       seven.doubleValue(),
                       eight.doubleValue(),
                       nine.doubleValue(),
                       ten.doubleValue(),
                       eleven.doubleValue(),
                       twelve.doubleValue(),
                       thirteen.doubleValue(),
                       fourteen.doubleValue(),
                       fifteen.doubleValue(),
                       sixteen.doubleValue(),
                       seventeen.doubleValue(),
                       eighteen.doubleValue(),
                       nineteen.doubleValue(),
                       twenty.doubleValue(),
                       twentyone.doubleValue(),
                       twentytwo.doubleValue(),
                       twentythree.doubleValue()


               });


                res.moveToNext();
            }

            if(flag==0){
                values.add(new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});

            }

            db.close();
        }

        return values;


    }


    public double getActualSleepTime(ArrayList<String> rows) throws ParseException
    {
        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Calendar today = Calendar.getInstance();
        Date todayWithZeroTime =formatter.parse(formatter.format(today.getTime()));
        double sum =0.0;
        for(int i=1 ; i<= 5 ; i++)
        {
            today.add(Calendar.DATE, -1);
            Date currentDate_parse =formatter.parse(formatter.format(today.getTime()));

            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "Select * from "+TABLE_NAME+" where date='" + currentDate_parse+"'";
            Cursor res =  db.rawQuery( selectQuery, null );
            res.moveToFirst();
            int flag =0;
            while(res.isAfterLast() == false)
            {
                flag=1;
                for(String r : rows){
                    sum+=res.getDouble(res.getColumnIndex(r));
                }
                res.moveToNext();
            }
            db.close();
        }

        return sum;


    }

    public double getTodayData() throws ParseException {

        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        Date today = new Date();
        Date todayWithZeroTime =formatter.parse(formatter.format(today));

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "Select * from "+TABLE_NAME+" where date='" + todayWithZeroTime+"'";
        Cursor res =  db.rawQuery( selectQuery, null );
        res.moveToFirst();
        double sum =0.0;
        while(res.isAfterLast() == false){
            sum+=res.getDouble(res.getColumnIndex("one"));
            sum+=res.getDouble(res.getColumnIndex("two"));
            sum+=res.getDouble(res.getColumnIndex("three"));
            sum+=res.getDouble(res.getColumnIndex("four"));
            sum+=res.getDouble(res.getColumnIndex("five"));
            sum+=res.getDouble(res.getColumnIndex("six"));
            sum+=res.getDouble(res.getColumnIndex("seven"));
            sum+=res.getDouble(res.getColumnIndex("eight"));
            sum+=res.getDouble(res.getColumnIndex("nine"));
            sum+=res.getDouble(res.getColumnIndex("ten"));
            sum+=res.getDouble(res.getColumnIndex("eleven"));
            sum+=res.getDouble(res.getColumnIndex("twelve"));
            sum+=res.getDouble(res.getColumnIndex("thirteen"));
            sum+=res.getDouble(res.getColumnIndex("fourteen"));
            sum+=res.getDouble(res.getColumnIndex("fifteen"));
            sum+=res.getDouble(res.getColumnIndex("sixteen"));
            sum+=res.getDouble(res.getColumnIndex("seventeen"));
            sum+=res.getDouble(res.getColumnIndex("eighteen"));
            sum+=res.getDouble(res.getColumnIndex("nineteen"));
            sum+=res.getDouble(res.getColumnIndex("twenty"));
            sum+=res.getDouble(res.getColumnIndex("twentyone"));
            sum+=res.getDouble(res.getColumnIndex("twentytwo"));
            sum+=res.getDouble(res.getColumnIndex("twentythree"));
            sum+=res.getDouble(res.getColumnIndex("zero"));
            res.moveToNext();
        }

        db.close();
        return sum;

    }




}
