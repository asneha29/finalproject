package com.example.s522560.project1;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends BaseActivity implements View.OnClickListener {

    SharedPreferences sharedPref;
    private static String TimePREFERENCES="MyTimePref";
    private Timer parsetimer = new Timer();
    private Timer timergetup = new Timer();
    private Timer timerfinished = new Timer();
    private Timer timerstart = new Timer();
    private TimerTask textRefresh;
    TextView fact;
    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setIcon(R.drawable.sleepicon1);
        getSupportActionBar().setTitle("  "+ "Insomiac");

        ImageView btn1 = (ImageView) findViewById(R.id.sheepFragBTN);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sheepbc);
        btn1.setImageBitmap(RoundedCornerShape.getRoundedCornerBitmap(bitmap, 20));
        btn1.setOnClickListener(this);


        ImageView btn2 = (ImageView) findViewById(R.id.musicFragBTN);
        Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.relaxmusic);
        btn2.setImageBitmap(RoundedCornerShape.getRoundedCornerBitmap(bitmap1, 20));
        btn2.setOnClickListener(this);

        ImageView btn3 = (ImageView) findViewById(R.id.bookFragBTN);
        Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.boringbookbc);
        btn3.setImageBitmap(RoundedCornerShape.getRoundedCornerBitmap(bitmap2, 20));
        btn3.setOnClickListener(this);

        ImageView btn4 = (ImageView) findViewById(R.id.graphFragBTN);
        Bitmap bitmap3 = BitmapFactory.decodeResource(getResources(), R.drawable.charticon);
        btn4.setImageBitmap(RoundedCornerShape.getRoundedCornerBitmap(bitmap3, 20));
        btn4.setOnClickListener(this);



        /// Updating Today Status
        MobileUsageDBHelper db = new MobileUsageDBHelper(this);
        try {
            double todayval = db.getTodayData();
            String val = Conversions.convertSecToHMS(todayval);
            System.out.println("dbval: "+ val);
            TextView t = (TextView) findViewById(R.id.todayStatusET);
            t.setText(String.valueOf(val));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        fact = (TextView) findViewById(R.id.facTXTET);
        getIsSleepTime();



    }


    public void updateFactCustomText(int flag){
        SharedPreferences sharedPref;
        sharedPref = getSharedPreferences(TimePREFERENCES, Context.MODE_PRIVATE);
        String name = sharedPref.getString("userName", "Buddy");
        if(flag == 0) {
            String s = "Hey " + name + ", it's bed time.\n Go back to Sleep!";
            fact.setText(s);
        }
        else if(flag ==1){
            String s = "Hey " + name + " \n It's time to Wake Up!";
            fact.setText(s);
        }
        else{
            String s = "Hey " + name + "\n Good Morning! \n Have a nice day!";
            fact.setText(s);
        }

    }

    public void getFact(){
        // Enable Local Datastore.
        ParseQuery<ParseObject> query = ParseQuery.getQuery("SleepTrivia");
        Random r = new Random();
        int rNum = r.nextInt(49);
        query.whereEqualTo("TriviaId", rNum);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, com.parse.ParseException e) {
                if (object == null) {
                    Log.d("score", "The getFirst request failed.");

                } else {
                    Log.d("score", "Retrieved the object.");
                    String fact = null;
                    try {
                        fact = new String( object.getString("TriviaName").getBytes("ISO-8859-1"), "UTF-8");
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }

                    Log.d("score", fact);
                    TextView f = (TextView) findViewById(R.id.facTXTET);
                    f.setText(fact);

                }
            }
        });
        return;
    }

    @Override
    public void onClick(View v) {

        Intent intent = new Intent(MainActivity.this, InsomniacSegments.class);

        switch (v.getId()){
            case R.id.sheepFragBTN:
                intent.putExtra("tab",0);
                startActivity(intent);

                break;

            case R.id.musicFragBTN:
                intent.putExtra("tab",1);
                startActivity(intent);

                break;

            case R.id.bookFragBTN:
                intent.putExtra("tab",2);
                startActivity(intent);

                break;

            case R.id.graphFragBTN:
                intent.putExtra("tab",3);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.setting) {
            Intent in = new Intent(this, AppSetting.class);
            startActivity(in);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public void onPause(){
        super.onPause();
        parsetimer.cancel();
    }

    public void onResume(){
        super.onResume();
        getIsSleepTime();

    }

    public  String getTimeFormat(int value){
        if(value > 9)
        {
            return String.valueOf(value);
        }
        else{
            String s = "0"+String.valueOf(value);
            return s;
        }
    }

    public boolean getIsSleepTime() {

        Calendar now = Calendar.getInstance();
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);
        int second = now.get(Calendar.SECOND);

        String state ="AM";
        if(hour > 12){
            state = "PM";
        }


        sharedPref = getSharedPreferences(TimePREFERENCES, Context.MODE_PRIVATE);

//
        int start_hour = sharedPref.getInt(getString(R.string.starttime_hour), 00);
        int start_minute = sharedPref.getInt(getString(R.string.starttime_minute), 00);
        int end_hour = sharedPref.getInt(getString(R.string.endtime_hour), 00);
        int end_minute = sharedPref.getInt(getString(R.string.endtime_minute), 00);

        String sam_pm = sharedPref.getString("StartAM-PM", "");
        String eam_pm = sharedPref.getString("EndAM-PM", "");

        String initialTime =getTimeFormat(start_hour)+":"+getTimeFormat(start_minute)+":00";
        String finalTime =getTimeFormat(end_hour)+":"+getTimeFormat(end_minute)+":00";
        String currentTime =getTimeFormat(hour)+":"+getTimeFormat(minute)+":00";

//       System.out.println("Checking HOut:" + initialTime + " lkfjlkd " + finalTime);

        try {
            boolean valid = false;
            //Start Time
            java.util.Date inTime = new SimpleDateFormat("HH:mm:ss").parse(initialTime);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(inTime);

            //Final time
            java.util.Date finTime = new SimpleDateFormat("HH:mm:ss").parse(finalTime);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(finTime);

            //Current Time
            java.util.Date checkTime = new SimpleDateFormat("HH:mm:ss").parse(currentTime);
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(checkTime);


            if (finalTime.compareTo(initialTime) < 0) {
                calendar2.add(Calendar.DATE, 1);
            }

            if(sam_pm.equals("PM") && eam_pm.equals("AM"))
            {
                if(state.equals("AM")){
                    calendar3.add(Calendar.DATE, 1);
                }
            }

            java.util.Date actualTime  = calendar3.getTime();


            System.out.println("CheckingHour: actual Time" + actualTime + "  Starttime   "+ calendar1.getTime() + "   endtime  "+ calendar2.getTime());
            System.out.println("CheckingHour: 1" + sam_pm + " " + eam_pm + " " + state);
            if ((actualTime.after(calendar1.getTime()) || actualTime.compareTo(calendar1.getTime()) == 0)
                    && actualTime.before(calendar2.getTime())) {
                valid = true;
            }


            System.out.println("Checking Time:" + valid);
            if(valid)
            {
                updateFactCustomText(0);


                int val1 =   ((int) (calendar2.getTimeInMillis() - calendar3.getTimeInMillis()));
                Calendar c = Calendar.getInstance();
                c.add(Calendar.SECOND, (val1/1000));
                Timer timer = new Timer();
                timer.schedule(new TaskExampleOnce(), c.getTime());

                if((val1/1000*60) <=15 )
                {
                    updateFactCustomText(1);
                }
                else{
//                    c.add(Calendar.SECOND, -900);
//                    Timer timer1 = new Timer();
//                    timer1.schedule(new TaskExampleOnce1(), c.getTime());
                }
                System.out.println("Timings: " + val1 + "  " + c.getTime() );

            }
            else{
                //Function to get random data from Parse.com
                SetParseTimer();


            }
            return valid;

        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }
    class TaskExampleOnce extends TimerTask{
        //This task will execute just once after seven seconds of starting the program
        public void run(){
            SetParseTimer();
        }
    }

    class TaskExampleOnce1 extends TimerTask{
        //This task will execute just once after seven seconds of starting the program
        public void run(){
            updateFactCustomText(1);
        }
    }


   public void SetParseTimer(){
       try {
           getFact();
           parsetimer = new Timer();
           textRefresh = new TimerTask() {
               @Override
               public void run() {
                   getFact();
               }
           };
           parsetimer.schedule(textRefresh, 20000, 20000);
       } catch (IllegalStateException e) {
           android.util.Log.i("Damn", "resume error");
       }
   }


}
