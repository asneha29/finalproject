package com.example.s522560.project1;

import android.view.View;

/**
 * Created by S522560 on 10/24/2015.
 */
public class HideStatusBar {


    public static void enableImmersiveMode(final View decorView) {
        decorView.setSystemUiVisibility(setSystemUiVisibility());
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    decorView.setSystemUiVisibility(setSystemUiVisibility());
                }
            }
        });
    }


    public static int setSystemUiVisibility() {
        return
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
             |   View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
              |   View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
             |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
               |  View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
    }
}
